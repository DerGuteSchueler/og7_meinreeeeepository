package arrayWDH;

import java.util.Arrays;

/**
 *
 * Übungsklasse zu Feldern
 *
 * @version 1.0 vom 05.05.2011
 * @author Asshabi&Sari
 */

public class felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };

	// Konstruktor
	public felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	// die Methode soll die größte Zahl der Liste zurückgeben
	public int maxElement() {
		int hilf1 = zahlenliste[1];
		for (int i = 0; i < zahlenliste.length; i++) {
			if (hilf1 < zahlenliste[i]) {
				hilf1 = zahlenliste[i];
			} else {
				hilf1 = hilf1;
			}
		}
		return hilf1;
	}

	// die Methode soll die kleinste Zahl der Liste zurückgeben
	public int minElement() {
		int hilf1 = zahlenliste[1];
		for (int i = 0; i < zahlenliste.length; i++) {
			if (hilf1 > zahlenliste[i]) {
				hilf1 = zahlenliste[i];
			} else {
				hilf1 = hilf1;
			}
		}
		return hilf1;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
	public int durchschnitt() {
		int hilf1 = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			hilf1 = hilf1 + zahlenliste[i];
		}
		hilf1 = Math.round((hilf1 / zahlenliste.length));
		return hilf1;
	}

	// die Methode soll die Anzahl der Elemente zurückgeben
	// der Befehl zahlenliste.length; könnte hierbei hilfreich sein
	public int anzahlElemente() {
		return zahlenliste.length;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		String a = Arrays.toString(zahlenliste);
		return a;
	}

	// die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {
		boolean wert = true;
		for (int i = 0; i < zahlenliste.length; i++) {
			if (zahl == zahlenliste[i]) {
				wert = true;
				break;
			} else {
				wert = false;
			}
		}
		return wert;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter übergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
		int wert = 0;
		int[] hilfsaraay = new int[zahlenliste.length - 1];
		for (int i = 0; i < zahlenliste.length; i++) {
			hilfsaraay[i] = i;
			if (zahl == zahlenliste[i]) {
				wert = hilfsaraay[i] + 1;
				break;
			} else {
				wert = -1;
			}
		}
		return wert;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public void sortiere() {
		Arrays.sort(zahlenliste);
	}

	public static void main(String[] args) {
		felder testenMeinerLösung = new felder();
		System.out.println(testenMeinerLösung.maxElement());
		System.out.println(testenMeinerLösung.minElement());
		System.out.println(testenMeinerLösung.durchschnitt());
		System.out.println(testenMeinerLösung.anzahlElemente());
		System.out.println(testenMeinerLösung.toString());
		System.out.println(testenMeinerLösung.istElement(9));
		System.out.println(testenMeinerLösung.getErstePosition(5));
		testenMeinerLösung.sortiere();
		System.out.println(testenMeinerLösung.toString());
	}
}
