package git_taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;
			
		// Display menu graphics
		System.out.print("Erste Zahl angeben: ");
		int zahl1 = myScanner.nextInt();
		System.out.print("Zweite Zahl angeben: ");
		int zahl2 = myScanner.nextInt();
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Dividieren     |");
		System.out.println("|        4. Multiplizieren |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println(zahl1 +" + " +zahl2 + " = " + ts.add(zahl1, zahl2));
			break;
		case '2':
			System.out.println(zahl1 +" - " +zahl2 + " = " + ts.sub(zahl1, zahl2));
			break;
		case '3':
			System.out.println(zahl1 +" / " +zahl2 + " = " + ts.div(zahl1, zahl2));
			break;
		case '4':
			System.out.println(zahl1 +" * " +zahl2 + " = " + ts.mul(zahl1, zahl2));
			break;
			
			
		  //  add your code here
		  
		  
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
			
		}
		myScanner.close();
	}

}
